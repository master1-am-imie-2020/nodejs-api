const mongoose = require('mongoose');

mongoose.connect('mongodb://localhost/Sylvestre', function(err) {
    console.log((err) ? err : 'Connection au mongo correct')
        /*
            if(err)
                console.log((err))
            else
                console.log('Connection au mongo correct')
        */
});

const userSchema = mongoose.Schema({
        nom: String,
        prenom: String,
        email: String,
        password: String,
        date_naissance: Date,
        lieu: String,
        createdAt: {
            type: Date,
            default: Date.now
        },
        updatedAt: {
            type: Date,
            default: Date.now
        },
    }),
    userModel = mongoose.model("users", userSchema)

exports.userModel = userModel