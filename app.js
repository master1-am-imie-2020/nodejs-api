/** START Import Module Node */
const express = require('express'),
    userModel = require('./models/users.js').userModel,
    cors = require('cors'),
    bodyParser = require('body-parser');
/** END Import Module Node */


const app = express();


app.use(function(request, response, next) {
    response.header("Access-Control-Allow-Origin", "*");
    response.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    response.header('Access-Control-Allow-Methods', 'PUT, POST, GET, DELETE');
    next();
});

let urlencodedParser = bodyParser.urlencoded({
    extended: false
})

/** 
 * @route /
 * @method GET
 */
app.get('/', function(req, res) {
    res.end("Coucou")
})

/** 
 * Affichage des utilisateurs
 * @route /users
 * @method GET
 */
app.get('/users', function(req, res) {
    userModel.find({}, (err, users) => retour(res, users))
})
app.get('/admin', function(req, res) {
    res.sendFile(__dirname + '/index.html')
})

/** 
 * Affichage d'utilisateur
 * @route /users/:id
 * @method GET
 */
app.get('/users/:id', function(req, res) {
    userModel.findById(req.params.id, (err, theUser) => retour(res, theUser))
})

/** 
 * Création d'utilisateur
 * @route /users
 * @method POST
 */
app.post('/users', urlencodedParser, function(req, res) {
    if (req.body.nom === undefined ||
        req.body.prenom === undefined ||
        req.body.email === undefined ||
        req.body.password === undefined ||
        req.body.date_naissance === undefined ||
        req.body.lieu === undefined) {
        retour(
            res, { error: true, message: "Données manquant" }, 402
        )
    } else {
        let user = new userModel({
            nom: req.body.nom,
            prenom: req.body.prenom,
            email: req.body.email,
            password: req.body.password,
            date_naissance: req.body.date_naissance,
            lieu: req.body.lieu,
        })
        userModel.find({ email: req.body.email }, (err, users) => {
            if (users.length != 0)
                retour(res, { error: true, message: "Email exist" }, 402)
            else {
                user.save().then(() => {
                    retour(res, req.body, 201)
                })
            }
        })
    }
})

/** 
 * Mise à jour d'utilisateur
 * @route /users/:id
 * @method PUT
 */
app.put('/users/:id', urlencodedParser, function(req, res) {
    let newUser = Object.create(null);

    if (req.body.nom !== undefined)
        newUser.nom = req.body.nom
    if (req.body.prenom !== undefined)
        newUser.prenom = req.body.prenom
    if (req.body.date_naissance !== undefined)
        newUser.date_naissance = req.body.date_naissance
    if (req.body.lieu !== undefined)
        newUser.lieu = req.body.lieu

    userModel.updateOne({ _id: req.params.id }, newUser, (err) => {
        retour(res, req.body)
    })
})

/** 
 * Suppression de tout les utilisateurs
 * @route /users
 * @method DELETE
 */
app.delete('/users', function(req, res) {

    userModel.deleteMany({}, (err) => {
        retour(res, { error: false, message: "All user delete" })
    })
})

/** 
 * Suppression d'un utilisateur via sont id
 * @route /users/:id
 * @method DELETE
 */
app.delete('/users/:id', function(req, res) {
    userModel.deleteOne({ _id: req.params.id }, (err) => {
        retour(res, { error: false, message: "User deleted" })
    })
})

function retour(res, data, code = 200) {
    res.writeHead(code, {
        'Content-Type': 'application/json'
    })
    res.end(JSON.stringify(data))
}

/** 
 * Ecoute du serveur sur le port 8080
 */
app.listen(8080, function() {
    console.log('Le serveur est run sur: http://localhost:8080/')
})