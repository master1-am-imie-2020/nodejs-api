const http = require("http"); // Chargement du module HTTP - Il permet la création d'un serveur sous NodeJs

/*
    var -> Bientot obs.
    let -> Diff entre le var et le let est la porter du scope
    const -> Donnée qui ne changera jamais

    interger(int) => 0 - 1 - 11 - 22 - 1993
    float(float) => 0.1 - 1.1 - 22.22 - 19.9303984
    boolean(bool) => True - Flase
    string(str) => "Hello!!!" - 'A' - "Zoubida"
    array(array) => [22, "fcefe", 3.3, [2,2]]
    object(object) => {
        "key": "valeur",
        "test": [22, "fcefe", 3.3, [2, 2]]
    }
    undefined(undefined) => Non definie
*/

let users = [{
    "id": 1,
    "nom": "Sylvestre",
    "prenom": "Mike",
    "date_naissance": "22/11/1993",
    "lieu": "Paris"
}, {
    "id": 2,
    "nom": "Zoubida",
    "prenom": "Adibouz",
    "date_naissance": "11/02/191",
    "lieu": "Paris"
}]

const serveur = http.createServer(function(req, res) {

    let url = req.url.split("/"); // Couper l'url par rapport au '/' afin de stocker chaque elements de mon url dans un tableau - [""]

    if (url[1] == "users") {
        if (url.length == 2) { // http://localhost:8081/users
            res.writeHead(200, {
                'Content-Type': 'application/json'
            })
            res.end(JSON.stringify(users))
        } else { // http://localhost:8081/users/Truc
            if (url[2] == "1") { // http://localhost:8081/users/1
                res.writeHead(200, {
                    'Content-Type': 'application/json'
                })
                res.end(JSON.stringify(users[0]))
            } else if (url[2] == "2") { // http://localhost:8081/users/2 http://sharemycode.fr/fo3
                res.writeHead(200, {
                    'Content-Type': 'application/json'
                })
                res.end(JSON.stringify(users[1]))
            } else { // http://localhost:8081/users/Truc
                res.writeHead(404)
                res.end()
            }
        }
    } else { // http://localhost:8081/Zoubida
        res.writeHead(404)
        res.end()
    }
});
const serveur2 = http.createServer(function(req, res) {
    console.log("Serveur run 2")
    res.end("Hello TATA !!!!")
});

serveur.listen(8080); // http://localhost:8080/
serveur2.listen(8081); // http://localhost:8081/



/*
    Fonction anonyme
        - Version 1 :
        function() {
            console.log("Serveur run")
        }
        - Version 2 :
        () => {
            console.log("Serveur run")
        }
*/